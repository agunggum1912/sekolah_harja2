<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $news = News::where('status', 'Aktif')->latest()->limit(3)->inRandomOrder()->get();
        return view('frontend.index', compact('news'));
    }

    public function tk()
    {
        return view('frontend.tk');
    }

    public function sd()
    {
        return view('frontend.sd');
    }

    public function smp()
    {
        return view('frontend.smp');
    }

    public function smk()
    {
        return view('frontend.smk');
    }

    public function news()
    {
        $news = News::where('status', 'Aktif')->get();
        return view('frontend.news', compact('news'));
    }
}
