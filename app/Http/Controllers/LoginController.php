<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;
use Validator;
use Session;
use DB;

class LoginController extends Controller
{
	public function getLogin(){
		return view('frontend.index');
	}

	public function postLogin(Request $request){
		// $this->validate($request, [
		// 	'email' => 'required|email',
		// 	'password' => 'required'
		// ]);

		$email 		= $request->email;
		$password 	= $request->password;

		$validasi = User::where('email',$email);

		$validator = Validator::make($request->all(),[
			'email'		=> 'required|email',
			'password'	=> 'required',
		]);

		if ($validator->fails()) {
			// $msg = [
			// 	'status'	=> 'errors',
			// 	'message'	=> 'Login gagal!'
			// ];
			return response()->json(['errors'=>$validator->errors()->all()]);
		// }elseif(Hash::check($password,['password' => $password])){
		// }elseif(is_null($validasi)){
		}elseif (Auth::guard('user')->attempt(['email' => $request->email, 'password' => $request->password])) {
			$msg = [
				'status'	=> true,
				'data'		=> '',
				'error'		=> ''
			];
			return response()->json($msg);
			// return redirect('/user');
		}else{
			$msg = [
				'status'	=> false,
				'data'		=> '',
				'error'		=> 'Wrong Email or Password'
			];
			return response()->json($msg);
			// return response()->json(['error']);
		}

		// $email = $request->input("emailLogin");
		// $password = $request->input("passwordLogin");
		// $users = DB::table(config('crudbooster.USER_TABLE'))->where("email", $email)->orWhere("no_hp", $email)->where('status', 'Aktif')->first();

		// if($users->status == null){
		// 	return redirect()->back()->with(['message'=>'Akun anda belum di aktivasi, Segera cek Inbox atau Spam E-mail anda untuk aktivasi!','message_type'=>'warning']);
		// 	Session::flush();
		// 	exit();
		// }
	}



	public function logout(){
		if (Auth::guard('user')->check()) {
			Auth::guard('user')->logout();
		}
		return redirect('/');
	}
}
