<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Register;
use Alert;

class RegisterController extends Controller
{
    public function create(){
        return view('frontend.index');
    }

    public function store(Request $request){
        $validator = \Validator::make($request->all(),[
            'email'                 => 'required|string|max:255|unique:cms_users|email',
            'nama'                  => 'required|string|max:255',
            'password'              => 'required|min:8|confirmed|alpha_num',
            'password_confirmation' => 'same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $register = new Register();
        $register->name=$request->get('nama');
        $register->email=$request->get('email');
        $register->password=Hash::make($request->get('password'));
        $register->save();

        return redirect('/');
        // Alert::success('Anda sudah berhasil membuat Akun','Selamat!');
        // return view('frontend.index');

    }

    // public function proses(Request $request){
    // 	$this->validate($request,[
    // 		'email' => 'required|string|max:255|unique:cms_users',
    // 		'nama' => 'required|string|max:255',
    // 		'password' => 'required',
    // 		'password_confirmation)' => 'required|string|min:8|confirmed',
    // 	]);

    // 	DB::table('cms_users')->insert([
    // 		'email' => $request->email,
    // 		'name' => $request->nama,
    // 		'password' => Hash::make($request->password)
    // 	]);
    // 	return redirect('/');
    // }
}
