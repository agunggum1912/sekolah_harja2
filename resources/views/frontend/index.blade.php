@extends('layouts.master')
@section('title', 'Harapan Jaya 2')

@section('content')
    <section class="flexslider">
        <ul class="slides">
        <li style="background-image: url(img/slider_1.jpg)" class="overlay">
            <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                <div class="probootstrap-slider-text text-center">
                    <h1 class="probootstrap-heading probootstrap-animate">Your Bright Future is Our Mission</h1>
                </div>
                </div>
            </div>
            </div>
        </li>
        <li style="background-image: url(img/slider_2.jpg)" class="overlay">
            <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                <div class="probootstrap-slider-text text-center">
                    <h1 class="probootstrap-heading probootstrap-animate">Education is Life</h1>
                </div>
                </div>
            </div>
            </div>

        </li>
        <li style="background-image: url(img/slider_3.jpg)" class="overlay">
            <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                <div class="probootstrap-slider-text text-center">
                    <h1 class="probootstrap-heading probootstrap-animate">Helping Each of Our Students Fulfill the Potential</h1>
                </div>
                </div>
            </div>
            </div>
        </li>
        </ul>
    </section>

    <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
        <div class="row">
            <div class="col-md-12 text-left section-heading probootstrap-animate">
            <h2></h2>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="probootstrap-flex-block">
                <div class="probootstrap-text probootstrap-animate">
                <h3>Tentang Kami</h3>
                <p>Yayasan Al ittihad didirikan pada tahun 1986 oleh ayahanda kami, H Tarmuzi, BA.  Seorang tokoh masyarakat Cengkareng Timur yang concern terhadap masalah-masalah  sosial pendidikan kaumnya, dengan Akte Notaris No.19 Tanggal 25 Desember 1986 oleh Notaris Tangerang bernama CH. Nuduri ATM, SH.</p>
                <p><a href="#" class="btn btn-primary">Selengkapnya</a></p>
                </div>
                <!-- <div class="probootstrap-image probootstrap-animate" style="background-image: url(img/slider_3.jpg)">
                <a href="https://vimeo.com/45830194" class="btn-video popup-vimeo"><i class="icon-play3"></i></a>
                </div> -->
                <div class="probootstrap-image probootstrap-animate" style="background-image: url(img/slider_3.jpg)">
                <video width="100%" height="100%" controls>
                    <source src="video/smk-harapan-jaya.mp4" type="video/mp4" class="btn-video popup-vimeo">
                </video>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Visi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Sebagai lembaga pendidikan bagi civitas akademika yang bermartabat dan terpercaya berlandaskan nilai-nilai Islam</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Misi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-6 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Membimibing dan membina tenaga pengajar bersikap ilmiah</h3>
                </div>
                </a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Mendidik siswa bertanggung jawab terhadap diri dan lingkungan</h3>
                </div>
                </a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Memfasilitasi kebutuhan pembelajaran Dan kegiatan pengembangan diri</h3>
                </div>
                </a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Mendorong warga kampus santun dan konsisten melaksanakan ibadah</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>

    <!-- <section class="probootstrap-section" id="probootstrap-counter">
        <div class="container">

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <div class="probootstrap-counter-wrap">
                <div class="probootstrap-icon">
                <i class="icon-users2"></i>
                </div>
                <div class="probootstrap-text">
                <span class="probootstrap-counter">
                    <span class="js-counter" data-from="0" data-to="20203" data-speed="5000" data-refresh-interval="50">1</span>
                </span>
                <span class="probootstrap-counter-label">Students Enrolled</span>
                </div>
            </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <div class="probootstrap-counter-wrap">
                <div class="probootstrap-icon">
                <i class="icon-user-tie"></i>
                </div>
                <div class="probootstrap-text">
                <span class="probootstrap-counter">
                    <span class="js-counter" data-from="0" data-to="139" data-speed="5000" data-refresh-interval="50">1</span>
                </span>
                <span class="probootstrap-counter-label">Certified Teachers</span>
                </div>
            </div>
            </div>
            <div class="clearfix visible-sm-block visible-xs-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <div class="probootstrap-counter-wrap">
                <div class="probootstrap-icon">
                <i class="icon-library"></i>
                </div>
                <div class="probootstrap-text">
                <span class="probootstrap-counter">
                    <span class="js-counter" data-from="0" data-to="99" data-speed="5000" data-refresh-interval="50">1</span>%
                </span>
                <span class="probootstrap-counter-label">Passing to Universities</span>
                </div>
            </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">

            <div class="probootstrap-counter-wrap">
                <div class="probootstrap-icon">
                <i class="icon-smile2"></i>
                </div>
                <div class="probootstrap-text">
                <span class="probootstrap-counter">
                    <span class="js-counter" data-from="0" data-to="100" data-speed="5000" data-refresh-interval="50">1</span>%
                </span>
                <span class="probootstrap-counter-label">Parents Satisfaction</span>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section> -->

    <!-- <section class="probootstrap-section probootstrap-section-colored probootstrap-bg probootstrap-custom-heading probootstrap-tab-section" style="background-image: url(img/slider_2.jpg)">
        <div class="container">
        <div class="row">
            <div class="col-md-12 text-center section-heading probootstrap-animate">
            <h2 class="mb0">Highlights</h2>
            </div>
        </div>
        </div>
        <div class="probootstrap-tab-style-1">
        <ul class="nav nav-tabs probootstrap-center probootstrap-tabs no-border">
            <li class="active"><a data-toggle="tab" href="#featured-news">Featured News</a></li>
            <li><a data-toggle="tab" href="#upcoming-events">Upcoming Events</a></li>
        </ul>
        </div>
    </section>

    <section class="probootstrap-section probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-12">

            <div class="tab-content">

                <div id="featured-news" class="tab-pane fade in active">
                <div class="row">
                    <div class="col-md-12">
                    <div class="owl-carousel" id="owl1">
                        <div class="item">
                        <a href="#" class="probootstrap-featured-news-box">
                            <figure class="probootstrap-media"><img src="img/img_sm_3.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                            <div class="probootstrap-text">
                            <h3>Tempora consectetur unde nisi</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima, ut.</p>
                            <span class="probootstrap-date"><i class="icon-calendar"></i>July 9, 2017</span>

                            </div>
                        </a>
                        </div>
                        <div class="item">
                        <a href="#" class="probootstrap-featured-news-box">
                            <figure class="probootstrap-media"><img src="img/img_sm_1.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                            <div class="probootstrap-text">
                            <h3>Tempora consectetur unde nisi</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, officia.</p>
                            <span class="probootstrap-date"><i class="icon-calendar"></i>July 9, 2017</span>

                            </div>
                        </a>
                        </div>
                        <div class="item">
                        <a href="#" class="probootstrap-featured-news-box">
                            <figure class="probootstrap-media"><img src="img/img_sm_2.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                            <div class="probootstrap-text">
                            <h3>Tempora consectetur unde nisi</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, dolores.</p>
                            <span class="probootstrap-date"><i class="icon-calendar"></i>July 9, 2017</span>
                            </div>
                        </a>
                        </div>
                        <div class="item">
                        <a href="#" class="probootstrap-featured-news-box">
                            <figure class="probootstrap-media"><img src="img/img_sm_3.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                            <div class="probootstrap-text">
                            <h3>Tempora consectetur unde nisi</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure, earum.</p>
                            <span class="probootstrap-date"><i class="icon-calendar"></i>July 9, 2017</span>
                            </div>
                        </a>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                    <p><a href="#" class="btn btn-primary">View all news</a></p>
                    </div>
                </div>
                </div>
                <div id="upcoming-events" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-12">
                    <div class="owl-carousel" id="owl2">
                        <div class="item">
                        <a href="#" class="probootstrap-featured-news-box">
                            <figure class="probootstrap-media"><img src="img/img_sm_3.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                            <div class="probootstrap-text">
                            <h3>Tempora consectetur unde nisi</h3>
                            <span class="probootstrap-date"><i class="icon-calendar"></i>July 9, 2017</span>
                            <span class="probootstrap-location"><i class="icon-location2"></i>White Palace, Brooklyn, NYC</span>
                            </div>
                        </a>
                        </div>
                        <div class="item">
                        <a href="#" class="probootstrap-featured-news-box">
                            <figure class="probootstrap-media"><img src="img/img_sm_1.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                            <div class="probootstrap-text">
                            <h3>Tempora consectetur unde nisi</h3>
                            <span class="probootstrap-date"><i class="icon-calendar"></i>July 9, 2017</span>
                            <span class="probootstrap-location"><i class="icon-location2"></i>White Palace, Brooklyn, NYC</span>
                            </div>
                        </a>
                        </div>
                        <div class="item">
                        <a href="#" class="probootstrap-featured-news-box">
                            <figure class="probootstrap-media"><img src="img/img_sm_2.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                            <div class="probootstrap-text">
                            <h3>Tempora consectetur unde nisi</h3>
                            <span class="probootstrap-date"><i class="icon-calendar"></i>July 9, 2017</span>
                            <span class="probootstrap-location"><i class="icon-location2"></i>White Palace, Brooklyn, NYC</span>
                            </div>
                        </a>
                        </div>
                        <div class="item">
                        <a href="#" class="probootstrap-featured-news-box">
                            <figure class="probootstrap-media"><img src="img/img_sm_3.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                            <div class="probootstrap-text">
                            <h3>Tempora consectetur unde nisi</h3>
                            <span class="probootstrap-date"><i class="icon-calendar"></i>July 9, 2017</span>
                            <span class="probootstrap-location"><i class="icon-location2"></i>White Palace, Brooklyn, NYC</span>
                            </div>
                        </a>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                    <p><a href="#" class="btn btn-primary">View all events</a></p>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section> -->

    <section class="probootstrap-section probootstrap-bg-white probootstrap-border-top">
        <div class="container">
        <!-- <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Our Featured Courses</h2>
            <p class="lead">Sed a repudiandae impedit voluptate nam Deleniti dignissimos perspiciatis nostrum porro nesciunt</p>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-6">
            <div class="probootstrap-service-2 probootstrap-animate">
                <div class="image">
                <div class="image-bg">
                    <img src="images/profil-tk.png" alt="Free Bootstrap Template by ProBootstrap.com">
                </div>
                </div>
                <div class="text">
                <h3>Taman Kanak-Kanak</h3>
                <br>
                <a href="tk.html" class="btn btn-primary">Kunjungi Halaman</a>
                </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="probootstrap-service-2 probootstrap-animate">
                <div class="image">
                <div class="image-bg">
                    <img src="images/profil-sd.png" alt="Free Bootstrap Template by ProBootstrap.com">
                </div>
                </div>
                <div class="text">
                <h3>Sekolah Dasar</h3>
                <br>
                <a href="sd.html" class="btn btn-primary">Kunjungi Halaman</a>
                </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="probootstrap-service-2 probootstrap-animate">
                <div class="image">
                <div class="image-bg">
                    <img src="img/img_sm_4.jpg" alt="Free Bootstrap Template by ProBootstrap.com">
                </div>
                </div>
                <div class="text">
                <h3>Sekolah Menengah Pertama</h3>
                <br>
                <a href="smp.html" class="btn btn-primary">Kunjungi Halaman</a>
                </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="probootstrap-service-2 probootstrap-animate">
                <div class="image">
                <div class="image-bg">
                    <img src="img/img_sm_1.jpg" alt="Free Bootstrap Template by ProBootstrap.com">
                </div>
                </div>
                <div class="text">
                <h3>Sekolah Menengah Kejuruan</h3>
                <br>
                <a href="#" class="btn btn-primary">Kunjungi Halaman</a>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>

    <!-- <section class="probootstrap-section probootstrap-bg probootstrap-section-colored probootstrap-testimonial" style="background-image: url(img/slider_4.jpg);">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Alumni Testimonial</h2>
            <p class="lead">Sed a repudiandae impedit voluptate nam Deleniti dignissimos perspiciatis nostrum porro nesciunt</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 probootstrap-animate">
            <div class="owl-carousel owl-carousel-testimony owl-carousel-fullwidth">
                <div class="item">
                <div class="probootstrap-testimony-wrap text-center">
                    <figure>
                    <img src="img/person_1.jpg" alt="Free Bootstrap Template by ProBootstrap.com">
                    </figure>
                    <blockquote class="quote">&ldquo;Design must be functional and functionality must be translated into visual aesthetics, without any reliance on gimmicks that have to be explained.&rdquo; <cite class="author"> &mdash; <span>Mike Fisher</span></cite></blockquote>
                </div>
                </div>
                <div class="item">
                <div class="probootstrap-testimony-wrap text-center">
                    <figure>
                    <img src="img/person_2.jpg" alt="Free Bootstrap Template by ProBootstrap.com">
                    </figure>
                    <blockquote class="quote">&ldquo;Creativity is just connecting things. When you ask creative people how they did something, they feel a little guilty because they didn’t really do it, they just saw something. It seemed obvious to them after a while.&rdquo; <cite class="author"> &mdash;<span>Jorge Smith</span></cite></blockquote>
                </div>
                </div>
                <div class="item">
                <div class="probootstrap-testimony-wrap text-center">
                    <figure>
                    <img src="img/person_3.jpg" alt="Free Bootstrap Template by ProBootstrap.com">
                    </figure>
                    <blockquote class="quote">&ldquo;I think design would be better if designers were much more skeptical about its applications. If you believe in the potency of your craft, where you choose to dole it out is not something to take lightly.&rdquo; <cite class="author">&mdash; <span>Brandon White</span></cite></blockquote>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section> -->
    <section class="probootstrap-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
                <h2>Berita Terbaru Harapan Jaya</h2>
                <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
                </div>
            </div>
        </div>
    </section>

<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Login</h5>
        <div class="alert alert-success" style="display: none;"></div>
        <div class="alert alert-danger" style="display: none;"></div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <form action="/login" method="post">
    <!-- {{ csrf_field() }} -->
    @csrf
    <div class="modal-body">
        <div class="form-group">
            <label for="emailLogin" class="col-form-label">Email</label>
            <input name="emailLogin" type="text" class="form-control" id="emailLogin">
        </div>
        <div class="form-group">
            <label for="passwordLogin" class="col-form-label">Password</label>
            <input name="passwordLogin" type="password" class="form-control" id="passwordLogin">
            <span>Belum punya akun? <a href="#" type="button" data-toggle="modal" data-target="#myModal" id="open" class="openRegis">Klik disini</a></span>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="ajaxLogin">Login</button>
    </div>
    </form>
    </div>
</div>
</div>

<!-- modal register -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Register</h5>
            <div class="alert alert-danger" style="display: none;"></div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form method="post" action="{{ url('regis')}}" id="form">
        <div class="modal-body">
            <!-- {{ csrf_field() }} -->
            <div class="form-group">
                <label for="email" class="col-form-label">Email</label>
                <input name="emailLoginl" type="text" class="form-control" value="{{old('email') }}" id="email">
                <!-- @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror -->
                <!-- @if($errors->has('email'))
                    <div class="text-danger">
                        {{ $errors->first('email')}}
                    </div>
                @endif -->
            </div>
            <div class="form-group">
                <label for="nama" class="col-form-label">Nama</label>
                <input name="nama" type="text" class="form-control" id="nama" value="{{ old('name') }}">
            </div>
            <div class="form-group">
                <label for="password" class="col-form-label">Password</label>
                <input name="password" type="password" class="form-control" id="password">
                <!-- @error('password')
                    <span class="invalid-feedback" role="alert">
                        {{ $message }}
                    </span>
                @enderror -->
                <!-- @if($errors->has('password'))
                    <div class="text-danger">
                        {{ $errors->first('password')}}
                    </div>
                @endif -->
            </div>
            <div class="form-group">
                <label for="password_confirmation" class="col-form-label">Konfirmasi Password</label>
                <input name="password_confirmation" type="password" class="form-control" id="password_confirmation">
                <!-- @if($errors->has('konfirmasi_password'))
                    <div class="text-danger">
                        {{ $errors->first('email')}}
                    </div>
                @endif -->
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button class="btn btn-primary" type="submit" id="ajaxSubmit">Register</button>
            <!-- <input type="submit" class="btn btn-primary" value="Register"> -->
        </div>
        </form>
        </div>
    </div>
</div>

@yield('footer')

    </div>
    <!-- END wrapper -->


@endsection
