@extends('layouts.master')
@section('title', 'Berita - Harapan Jaya 2')

@section('content')
<section class="probootstrap-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Seputar Harapan Jaya</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            @foreach ($news as $item)
            <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                    <figure class="probootstrap-media">
                    <img src="{{ asset( $item->large_image ) }}" alt="{{ $item->title }}" class="img-responsive mh-100"></figure>
                    <div class="probootstrap-text">
                    <h3>{{ $item->title }}</h3>
                    <span class="probootstrap-date"><i class="icon-calendar"></i>July 9, 2017</span>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
