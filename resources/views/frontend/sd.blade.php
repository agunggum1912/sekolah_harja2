@extends('layouts.master')
@section('title', 'SD - Harapan Jaya 2')

@section('content')
    <section class="probootstrap-section">
        <div class="container">

        <div class="col-md-6">
            <p>
            <img src="images/profil-sd.png" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive">
            </p>
        </div>
        <div class="col-md-6 col-md-push-1">
            <h2>Mengapa SD Harapan Jaya</h2>
            <p>Sekolah Dasar Harapan Jaya berupaya menyelenggarakan konsep pendidikan pra sekolah dengan menggunakan pembelajaran melalui sentra. Kegiatan-kegiatan yang ada didalam sentra disiapkan untuk membangun akhlakul karimah, kecerdasan jamak dan enam domain berpikir pada anak (Aestetika, Afeksi, Bahasa, Kognisi, Fisik dan Sosial).</p>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Visi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Terwujudnya lembaga pendidikan yang mampu mencetak generasi muslim yang berakhlak mulia, cerdas dan berkarakter </h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Target Lulusan</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Diterima di sekolah favorit</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Hafal minimal juz 30.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Hafal 20 hadist pilihan.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Terbiasa berakhlak islami.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Terbiasa bahasa asing.</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Program Kegiatan</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_1.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                <div class="probootstrap-text">
                <h3>Kurikulum Terpadu</h3>
                </div>
            </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_2.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                <div class="probootstrap-text">
                <h3>Ekstrakurikuler</h3>
                </div>
            </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_2.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                <div class="probootstrap-text">
                <h3>Penguatan Konsep Diri</h3>
                </div>
            </a>
            </div>
            <div class="clearfix visible-sm-block visible-xs-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_3.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                <div class="probootstrap-text">
                <h3>Pembelajaran Terpadu</h3>
                </div>
            </a>
            </div>
        </div>
        </div>
    </section>

    <!-- <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Why Choose Enlight School</h2>
            <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Consectetur Adipisicing</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>
            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Aliquid Dolorum Saepe</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>
            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Eveniet Tempora Anisi</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Laboriosam Quod Dignissimos</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>

            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Asperiores Maxime Modi</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>

            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Libero Maxime Molestiae</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>

            </div>
        </div>
        </div>
    </section> -->

    <!-- <section class="probootstrap-cta">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 class="probootstrap-animate" data-animate-effect="fadeInRight">Get your admission now!</h2>
            <a href="#" role="button" class="btn btn-primary btn-lg btn-ghost probootstrap-animate" data-animate-effect="fadeInLeft">Enroll</a>
            </div>
        </div>
        </div>
    </section> -->

    <section class="probootstrap-cta">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 class="probootstrap-animate" data-animate-effect="fadeInRight">Pendaftaran Siswa SD Baru!</h2>
            <a href="https://api.whatsapp.com/send?phone=6281386235324&text=Assalamualaikum%20wr%20wb..%0AHalo%20Sekolah%20Harapan%20Jaya%202%2C%20saya%20mau%20melakukan%20pendaftaran%2C%20mohon%20info%20lebih%20lanjut..%0A%0ANama%20Anak%3A%0ANama%20Orang%20Tua%3A%0AAlamat%3A%0AAsal%20Sekolah%3A%0AEmail%3A%0A%0ATerima%20kasih."
            role="button" class="btn btn-primary btn-lg btn-ghost probootstrap-animate" data-animate-effect="fadeInLeft">Daftar Sekarang</a>
            </div>
        </div>
        </div>
    </section>

    @yield('footer')

    </div>
    <!-- END wrapper -->
@endsection
