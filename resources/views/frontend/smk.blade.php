@extends('layouts.master')
@section('title', 'SMK - Harapan Jaya 2')

@section('content')
    <section class="probootstrap-section">
        <div class="container">

        <div class="col-md-6">
            <p>
            <img src="img/img_sm_3.jpg" alt="SMP Harapan Jaya" class="img-responsive">
            </p>
        </div>
        <div class="col-md-6 col-md-push-1">
            <h2>Mengapa SMK Harapan Jaya</h2>
            <p>Sejalan dengan pelaksanaan misi Pendidikan Agama Islam Kementriaan Agama RI. yaitu mengembangkan kurikulum Pendidikan Agama Islam dan Meningkatkan mutu Lulusan siswa maka SMP Harapan Jaya 2 akan membuat Program yaitu Taman Pendidikan Al-Qur’an dan Gema Al-Qur’an.</p>
        </div>
        </div>
    </section>

    <!-- <section class="probootstrap-section probootstrap-bg-white probootstrap-border-top">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>MOTTO</h2>
            <p class="lead">Semangat, Percaya Diri, Terampil dan Cerdas</p>
            </div>
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>FILOSOFI</h2>
            <p class="lead">Membentuk Manusia Unggul Yang ber IPTEK Dan IMTAQ</p>
            </div>
        </div>
        </div>
    </section> -->
    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="probootstrap-service-2 probootstrap-animate">
                <div class="image">
                <div class="image-bg">
                    <img src="img/img_sm_1.jpg" alt="Free Bootstrap Template by ProBootstrap.com">
                </div>
                </div>
                <div class="text">
                <h3>Otomatisasi dan Tata Kelola Perkantoran (OTKP)</h3>
                <br>
                <p>- Recepsionis</p>
                <p>- Sekretaris Junior</p>
                <p>- SPG</p>
                <p>- Tenaga Pemasaran</p>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Visi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Menghasilkan lulusan untuk menjadi tenaga Sekretaris Junior yang luwes , terampil, dan berakhlak mulia yang sesuai dengan tuntutan dunia usaha / dunia industri.</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Misi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Mendidik siswa dalam bidang bisnis dan manajemen khususnya program keahlian Otomatisasi Dan Tata Kelola Perkantoran agar dapat menjadi tenaga yang siap bekerja sesuai dengan tamatan.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Mendidik siswa agar mampu memilih karir, berkompetensi, dan mengembangkan sikap professional.</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>


    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="probootstrap-service-2 probootstrap-animate">
                <div class="image">
                <div class="image-bg">
                    <img src="img/img_sm_1.jpg" alt="Free Bootstrap Template by ProBootstrap.com">
                </div>
                </div>
                <div class="text">
                <h3>Akuntansi dan Keuangan Lembaga (AKL)</h3>
                <br>
                <p>- Staff Tata Usaha</p>
                <p>- Staff Administrasi Keuangan</p>
                <p>- Account Executive</p>
                <p>- Wirausahawan</p>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Visi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Menghasilkan kelulusan untuk menjadi tenaga akuntansi yang terampil, disiplin, tanggung jawab, dan berakhlak mulia yang sesuai dengan tuntutan dunia usaha / dunia industri.</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Misi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Mendidik siswa dalam bidang bisnis dan manajemen khususnya program keahlian Akuntansi agar dapat bekerja dengan baik dan mandiri.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Mendidik siswa agar mampu memilih karir, berkompetensi, dan mengembangkan sikap professional.</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>


    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="probootstrap-service-2 probootstrap-animate">
                <div class="image">
                <div class="image-bg">
                    <img src="img/img_sm_1.jpg" alt="Free Bootstrap Template by ProBootstrap.com">
                </div>
                </div>
                <div class="text">
                <h3>Teknik Komputer dan Jaringan (TKJ)</h3>
                <br>
                <p>- Administrasi Jaringan</p>
                <p>- Teknisi Komputer / Laptop</p>
                <p>- Internet Service</p>
                <p>- Client Server Installation</p>
                <p>- IT Consultan</p>
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Visi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Menghasilkan lulusan yang memiliki kompetensi unggul di bidang teknik komputer dan jaringan, mandiri, disiplin dan berakhlak mulia yang sesuai dengan tuntutan dunia usaha dan dunia industri.</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Misi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Menyelenggarakan kegiatan belajar dan mengajar yang menitik beratkan kepada aspek pengetahuan, keterampilan dan sikap secara berimbang. yang dilandasi dengan kedisiplinan, kemandirian dan budi pekerti luhur.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Meningkatkan kualitas pendidik melalui sertifikasi kompetensi dan on the job training di berbagai lembaga diklat dan DUDI.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Mengembangan potensi siswa melalui pembinaan mental dan kedisiplinan.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Menjalin kerja sama dengan dunia industri yang sesuai sehingga penyelenggaraan pendidikan sistem ganda dapat direalisasikan dengan tepat.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Melaksanakan pelayanan prima dalam pengelolaan kompetensi keahlian.</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Galeri SMK Harapan Jaya</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_1.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
            </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_2.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
            </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_2.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
            </a>
            </div>
            <div class="clearfix visible-sm-block visible-xs-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_3.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
            </a>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-cta">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 class="probootstrap-animate" data-animate-effect="fadeInRight">Pendaftaran Siswa SMK Baru!</h2>
            <a href="https://api.whatsapp.com/send?phone=6282122332800&text=Assalamualaikum%20wr%20wb..%0AHalo%20Sekolah%20Harapan%20Jaya%202%2C%20saya%20mau%20melakukan%20pendaftaran%2C%20mohon%20info%20lebih%20lanjut..%0A%0ANama%20Anak%3A%0ANama%20Orang%20Tua%3A%0AAlamat%3A%0AAsal%20Sekolah%3A%0AEmail%3A%0A%0ATerima%20kasih."
            role="button" class="btn btn-primary btn-lg btn-ghost probootstrap-animate" data-animate-effect="fadeInLeft">Daftar Sekarang</a>
            </div>
        </div>
        </div>
    </section>

    @yield('footer')

    </div>
    <!-- END wrapper -->


@endsection
