@extends('layouts.master')
@section('title', 'SMP - Harapan Jaya 2')

@section('content')
    <section class="probootstrap-section">
        <div class="container">

        <div class="col-md-6">
            <p>
            <img src="img/img_sm_3.jpg" alt="SMP Harapan Jaya" class="img-responsive">
            </p>
        </div>
        <div class="col-md-6 col-md-push-1">
            <h2>Mengapa SMP Harapan Jaya</h2>
            <p>Sejalan dengan pelaksanaan misi Pendidikan Agama Islam Kementriaan Agama RI. yaitu mengembangkan kurikulum Pendidikan Agama Islam dan Meningkatkan mutu Lulusan siswa maka SMP Harapan Jaya 2 akan membuat Program yaitu Taman Pendidikan Al-Qur’an dan Gema Al-Qur’an.</p>
        </div>
        </div>
    </section>

    <!-- <section class="probootstrap-section probootstrap-bg-white probootstrap-border-top">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>MOTTO</h2>
            <p class="lead">Semangat, Percaya Diri, Terampil dan Cerdas</p>
            </div>
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>FILOSOFI</h2>
            <p class="lead">Membentuk Manusia Unggul Yang ber IPTEK Dan IMTAQ</p>
            </div>
        </div>
        </div>
    </section> -->

    <section class="probootstrap-section probootstrap-bg-white probootstrap-border-top text-center">
            <div class="col-md-12">
            <div class="section-heading probootstrap-service-2 probootstrap-animate">
                <div class="text text-center">
                <h2 class="text-center">MOTTO</h2>
                <p>Semangat, Percaya Diri, Terampil dan Cerdas</p>
                </div>
            </div>
            </div>
            <div class="col-md-12">
            <div class="section-heading probootstrap-service-2 probootstrap-animate">
                <div class="text text-center">
                <h2 class="text-center">FILOSOFI</h2>
                <p>Membentuk Manusia Unggul Yang ber IPTEK Dan IMTAQ</p>
                </div>
            </div>
            </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Visi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Menyiapkan siswa yang unggul dalam prestasi berdasarkan IPTEK dan IMTAQ serta ramah lingkungan dan hidup sehat.</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Misi</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Menyelenggarakan Pendidikan Dan Proses belajar mengajar yang bermutu dan bermanfaat yang didukung oleh Sarana dan Prasarana yang memadai.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Menyiapkan peserta didik menjadi lebih cerdas dan mandiri dalam sikap, kepribadian, pemikiran serta tindakan.</h3>
                </div>
                </a>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12 probootstrap-animate">
                <a href="#" class="probootstrap-featured-news-box">
                <div class="probootstrap-text text-center">
                    <h3>Mewujudkan lingkungan sekolah yang "Bersih, Indah, Hijau dan Sehat".</h3>
                </div>
                </a>
            </div>
            </div>
        </div>
        </div>
    </section>

    <section class="probootstrap-cta">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 class="probootstrap-animate" data-animate-effect="fadeInRight">Pendaftaran Siswa SMP Baru!</h2>
            <a href="https://api.whatsapp.com/send?phone=628161941777&text=Assalamualaikum%20wr%20wb..%0AHalo%20Sekolah%20Harapan%20Jaya%202%2C%20saya%20mau%20melakukan%20pendaftaran%2C%20mohon%20info%20lebih%20lanjut..%0A%0ANama%20Anak%3A%0ANama%20Orang%20Tua%3A%0AAlamat%3A%0AAsal%20Sekolah%3A%0AEmail%3A%0A%0ATerima%20kasih."
            role="button" class="btn btn-primary btn-lg btn-ghost probootstrap-animate" data-animate-effect="fadeInLeft">Daftar Sekarang</a>
            </div>
        </div>
        </div>
    </section>

    @yield('footer')

</div>
<!-- END wrapper -->


@endsection
