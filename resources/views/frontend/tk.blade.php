@extends('layouts.master')
@section('title', 'TK - Harapan Jaya 2')

@section('content')
    <section class="probootstrap-section">
        <div class="container">

        <div class="col-md-6">
            <p>
                <img src="images/profil-tk.png" alt="TK Harapan Jaya" class="img-pendidikan img-2">
                <img src="images/profil-tk.png" alt="TK Harapan Jaya" class="img-pendidikan img-1">
            </p>
        </div>
        <div class="col-md-6 col-md-push-1">
            <h2 class="font-1">Mengapa TK Harapan Jaya</h2>
            <p class="font-2">Taman Kanak-kanak Harapan Jaya berupaya menyelenggarakan konsep pendidikan pra sekolah dengan menggunakan pembelajaran melalui sentra. Kegiatan-kegiatan yang ada didalam sentra disiapkan untuk membangun akhlakul karimah, kecerdasan jamak dan enam domain berpikir pada anak (Aestetika, Afeksi, Bahasa, Kognisi, Fisik dan Sosial).</p>
        </div>
        </div>
    </section>
    <br><br>

    <section class="probootstrap-cta">
        <div class="container">
            <div class="col-md-12">
                <div class="text-center">
                    <span class="font-3">PROGRAM UNGGULAN</span>
                </div>
                <div class="col-md-4">
                    <div class="kotak">
                        <div class="logo-pu-red">
                            <i class="fas fa-balance-scale"></i>
                        </div>
                        <div class="text-center">
                            <span class="font-green">JUDUL PU</span>
                        </div>
                        <div class="text-center">
                            <p class="font-4">ini adalah isi dalam keterangan program unggulan sesuai judulnya, selebih lengkapnya bisa klik button biar lebih lengkap ada apa aja</p>
                        </div>
                        <div class="col text-center">
                            <a href="" class="button button-red">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="kotak">
                        <div class="logo-pu-green">
                            <i class="fas fa-book-reader"></i>
                        </div>
                        <div class="text-center">
                            <span class="font-grey">JUDUL PU</span>
                        </div>
                        <div class="text-center">
                            <p class="font-4">ini adalah isi dalam keterangan program unggulan sesuai judulnya, selebih lengkapnya bisa klik button biar lebih lengkap ada apa aja</p>
                        </div>
                        <div class="col text-center">
                            <a href="" class="button button-green">Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="kotak">
                        <div class="logo-pu-grey">
                            <i class="fas fa-laptop"></i>
                        </div>
                        <div class="text-center">
                            <span class="font-red">JUDUL PU</span>
                        </div>
                        <div class="text-center">
                            <p class="font-4">ini adalah isi dalam keterangan program unggulan sesuai judulnya, selebih lengkapnya bisa klik button biar lebih lengkap ada apa aja</p>
                        </div>
                        <div class="col text-center">
                            <a href="" class="button button-grey">Selengkapnya</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br><br>

    <section class="probootstrap-section">
        <div class="container text-center">
            <span class="font-6">KESISWAAN</span> <span class="font-5">TK</span>
            <br><br>
            <div class="col-md-12">
                <span>Ini adalah isi perkataan dari kesiswaan. Ini adalah isi perkataan dari kesiswaan. Ini adalah isi perkataan dari kesiswaan.Ini adalah isi perkataan dari kesiswaan. Ini adalah isi perkataan dari kesiswaan. Ini adalah isi perkataan dari kesiswaan.</span>
            </div>
            <br><br>

            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="side-left">
                        <img class="img-kesiswaan" src="images/profil-tk.png" alt="TK Harapan Jaya">
                    </div>
                    <div class="side-right bg-card text-left">
                        <div class="margin-b-1">
                            <span class="font-judul-left">Ini Judul</span>
                            <br>
                        </div>
                        <div class="margin-b-1">
                            <span class="font-sub-left">Sub judul</span>
                            <br>
                        </div>
                        <span class="font-7">Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. </span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="side-left">
                        <img class="img-kesiswaan" src="images/profil-tk.png" alt="TK Harapan Jaya">
                    </div>
                    <div class="side-right bg-card text-left">
                        <div class="margin-b-1">
                            <span class="font-judul-left">Ini Judul</span>
                            <br>
                        </div>
                        <div class="margin-b-1">
                            <span class="font-sub-left">Time:</span>
                            <span class="font-7">13-01-2021 22:00</span>
                            <br>
                        </div>
                        <span class="font-7">Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. </span>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="side-left">
                        <img class="img-kesiswaan" src="images/profil-tk.png" alt="TK Harapan Jaya">
                    </div>
                    <div class="side-right bg-card text-left">
                        <div class="margin-b-1">
                            <span class="font-judul-left">Ini Judul</span>
                            <br>
                        </div>
                        <div class="margin-b-1">
                            <span class="font-sub-left">Sub judul</span>
                            <br>
                        </div>
                        <span class="font-7">Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. </span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="side-left">
                        <img class="img-kesiswaan" src="images/profil-tk.png" alt="TK Harapan Jaya">
                    </div>
                    <div class="side-right bg-card text-left">
                        <div class="margin-b-1">
                            <span class="font-judul-left">Ini Judul</span>
                            <br>
                        </div>
                        <div class="margin-b-1">
                            <span class="font-sub-left">Time:</span>
                            <span class="font-7">13-01-2021 22:00</span>
                            <br>
                        </div>
                        <span class="font-7">Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. Ini adalah isi dari kesiswaan. </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br><br>

    <section class="probootstrap-section">
        <div class="container">
            <div class="box-left">
                <span class="font-11">VISI</span>
            </div>
            <div class="box-right">
                <div class="font-10">
                    <i class="fas fa-check check-list"></i>
                    <span >Membimibing dan membina tenaga pengajar bersikap ilmiah</span>
                </div>
                <div class="font-10">
                    <i class="fas fa-check check-list"></i>
                    <span >Membimibing dan membina tenaga pengajar bersikap ilmiah</span>
                </div>
                <div class="font-10">
                    <i class="fas fa-check check-list"></i>
                    <span >Membimibing dan membina tenaga pengajar bersikap ilmiah</span>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
            <div class="box-left">
                <span class="font-11">MISI</span>
            </div>
            <div class="box-right">
                <div class="font-10">
                    <i class="fas fa-check check-list"></i>
                    <span >Membimibing dan membina tenaga pengajar bersikap ilmiah</span>
                </div>
                <div class="font-10">
                    <i class="fas fa-check check-list"></i>
                    <span >Membimibing dan membina tenaga pengajar bersikap ilmiah</span>
                </div>
                <div class="font-10">
                    <i class="fas fa-check check-list"></i>
                    <span >Membimibing dan membina tenaga pengajar bersikap ilmiah</span>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
            <div class="box">
                <div class="box-left-2">
                    <span class="font-11">TARGET LULUSAN</span>
                </div>
                <div class="box-right">
                    <div class="font-10">
                        <i class="fas fa-check check-list"></i>
                        <span >Membimibing dan membina tenaga pengajar bersikap ilmiah</span>
                    </div>
                    <div class="font-10">
                        <i class="fas fa-check check-list"></i>
                        <span >Membimibing dan membina tenaga pengajar bersikap ilmiah</span>
                    </div>
                    <div class="font-10">
                        <i class="fas fa-check check-list"></i>
                        <span >Membimibing dan membina tenaga pengajar bersikap ilmiah</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br><br>

    <section class="probootstrap-section">
        <div class="container">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="col-md-6">
                        <div class="left-float">
                            <div class="kotak-3">
                                <h2 class="font-8">Prestasi</h2>
                                <span class="font-9">konten sekilas isi dari Target Lulusan TK Harapan Jaya Tangerang, bisa mendidik anak menjadi pribadi yang lebih baik.</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="right-float">
                            <div class="kotak-2">
                                <div class="logo-tl">
                                    <i class="fas fa-robot"></i>
                                </div>
                                <div class="text-center">
                                    <span class="font-green">Judul Lulusan</span><br>
                                    <span>isi sedikit konten dari Judul Lulusan ini bisa di kasih keterangan.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        <div class="left-float">
                            <div class="kotak-2">
                                <div class="logo-tl">
                                    <i class="fas fa-robot"></i>
                                </div>
                                <div class="text-center">
                                    <span class="font-green">Judul Lulusan</span><br>
                                    <span>isi sedikit konten dari Judul Lulusan ini bisa di kasih keterangan.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="right-float">
                            <div class="kotak-2">
                                <div class="logo-tl">
                                    <i class="fas fa-robot"></i>
                                </div>
                                <div class="text-center">
                                    <span class="font-green">Judul Lulusan</span><br>
                                    <span>isi sedikit konten dari Judul Lulusan ini bisa di kasih keterangan.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Program Kegiatan</h2>
            <!-- <p class="lead">Sekolah Harapan Jaya</p> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_1.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                <div class="probootstrap-text">
                <h3>Toddler</h3>
                </div>
            </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_2.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                <div class="probootstrap-text">
                <h3>Kelompok Bermain</h3>
                </div>
            </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_2.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                <div class="probootstrap-text">
                <h3>TK A</h3>
                </div>
            </a>
            </div>
            <div class="clearfix visible-sm-block visible-xs-block"></div>
            <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 probootstrap-animate">
            <a href="#" class="probootstrap-featured-news-box">
                <figure class="probootstrap-media"><img src="img/img_sm_3.jpg" alt="Free Bootstrap Template by ProBootstrap.com" class="img-responsive"></figure>
                <div class="probootstrap-text">
                <h3>TK B</h3>
                </div>
            </a>
            </div>
        </div>
        </div>
    </section>



    <!-- <section class="probootstrap-section">
        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
            <h2>Why Choose Enlight School</h2>
            <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Consectetur Adipisicing</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>
            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Aliquid Dolorum Saepe</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>
            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Eveniet Tempora Anisi</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Laboriosam Quod Dignissimos</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>

            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Asperiores Maxime Modi</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>

            <div class="service left-icon probootstrap-animate">
                <div class="icon"><i class="icon-checkmark"></i></div>
                <div class="text">
                <h3>Libero Maxime Molestiae</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                </div>
            </div>

            </div>
        </div>
        </div>
    </section> -->

    <!-- <section class="probootstrap-cta">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 class="probootstrap-animate" data-animate-effect="fadeInRight">Get your admission now!</h2>
            <a href="#" role="button" class="btn btn-primary btn-lg btn-ghost probootstrap-animate" data-animate-effect="fadeInLeft">Enroll</a>
            </div>
        </div>
        </div>
    </section> -->
    <section class="probootstrap-cta">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 class="probootstrap-animate" data-animate-effect="fadeInRight">Pendaftaran Siswa TK Baru!</h2>
            <a href="https://api.whatsapp.com/send?phone=6281314566393&text="
            role="button" class="btn btn-primary btn-lg btn-ghost probootstrap-animate" data-animate-effect="fadeInLeft">Daftar Sekarang</a>
            </div>
        </div>
        </div>
    </section>

    @yield('footer')

</div>
<!-- END wrapper -->


@endsection
