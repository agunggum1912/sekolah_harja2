<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title', 'Harapan Jaya')</title>
<meta name="_token" content="{{csrf_token()}}" />
<link rel="stylesheet" href="https://use.fontawesome.com/aad365026b.css">
<meta name="description" content="Free Bootstrap Theme by ProBootstrap.com">
<meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700|Open+Sans" rel="stylesheet">
<link rel="shortcut icon" href="images/favicon.png">
<link rel="stylesheet" href="css/styles-merged.css">
<link rel="stylesheet" href="css/style.min.css">
<link rel="stylesheet" href="css/custom.css">
<script src="https://kit.fontawesome.com/8599349197.js" crossorigin="anonymous"></script>
