<footer class="probootstrap-footer probootstrap-bg">
    <div class="container">
    <div class="row">
        <div class="col-md-4">
        <div class="probootstrap-footer-widget">
            <h3>Tentang Sekolah</h3>
            <p>Yayasan Al ittihad didirikan pada tahun 1986 oleh ayahanda kami, H Tarmuzi, BA.  Seorang tokoh masyarakat Cengkareng Timur yang concern terhadap masalah-masalah  sosial pendidikan kaumnya, dengan Akte Notaris No.19 Tanggal 25 Desember 1986 oleh Notaris Tangerang bernama CH. Nuduri ATM, SH.</p>
        </div>
        </div>
        <div class="col-md-4">
        <div class="probootstrap-footer-widget">
            <h3>Informasi Kontak</h3>
            <ul class="probootstrap-contact-info">
            <li><i class="icon-location2"></i> <span>Yayasan Al - Itihad, Jl. H. Mansyur No.25, RT.005/RW.005, Gondrong, Kec. Cipondoh, Kota Tangerang, Banten 15146</span></li>
            <li><i class="icon-mail"></i><span>info@harapanjaya.com</span></li>
            <li><i class="icon-phone2"></i><span>(021) 55755671</span></li>
            </ul>
        </div>
        </div>
        <div class="col-md-4">
        <div class="probootstrap-footer-widget">
            <h3>Temukan Kami di Maps</h3>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.4858195450247!2d106.69317781458861!3d-6.199456095512026!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f9be6966bc75%3A0xc01200e51198748a!2sSekolah%20Menengah%20Kejuruan%20(Sekolah%20Menengah%20Ekonomi%20Atas)%20Harapan%20Jaya%202!5e0!3m2!1sen!2sid!4v1606633038275!5m2!1sen!2sid" width="90%" height="150%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
        </div>

    </div>
    <!-- END row -->

    </div>

    <div class="probootstrap-copyright">
    <div class="container">
        <div class="row">
        <div class="col-md-8 text-left">
            <!-- <p>&copy; 2017 <a href="https://probootstrap.com/">ProBootstrap:Enlight</a>. All Rights Reserved. Designed &amp; Developed with <i class="icon icon-heart"></i> by <a href="https://probootstrap.com/">ProBootstrap.com</a></p> -->
        </div>
        <div class="col-md-4 probootstrap-back-to-top">
            <p><a href="#" class="js-backtotop">Back to top <i class="icon-arrow-long-up"></i></a></p>
        </div>
        </div>
    </div>
    </div>
</footer>
