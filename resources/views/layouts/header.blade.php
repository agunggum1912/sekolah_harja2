<div class="probootstrap-header-top">
    <div class="container">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9 probootstrap-top-quick-contact-info">
        <!-- <span><i class="icon-location2"></i>Yayasan Al - Itihad, Jl. H. Mansyur No.25, RT.005/RW.005, Gondrong, Kec. Cipondoh, Kota Tangerang, Banten 15146</span> -->
        <span><i class="icon-phone2"></i>info@harapanjaya.com</span>
        <span><i class="icon-mail"></i>(021) 55755671</span>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 probootstrap-top-social">
        <ul>
            <li><a href="#"><i class="icon-facebook2"></i></a></li>
            <li><a href="#"><i class="icon-instagram2"></i></a></li>
            <li><a href="#"><i class="icon-youtube"></i></a></li>
            <li>
                <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#register">Register</button> -->
            </li>
        </ul>
        </div>
    </div>
    </div>
</div>
<nav class="navbar navbar-default probootstrap-navbar">
    <div class="container">
    <div class="navbar-header">
        <div class="btn-more js-btn-more visible-xs">
        <a href="#"><i class="icon-dots-three-vertical "></i></a>
        </div>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html" title="ProBootstrap:Enlight">Enlight</a>
    </div>

    <div id="navbar-collapse" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
        <li><a href="/">Beranda</a></li>
        <li><a href="/news">Berita</a></li>
        <!-- <li><a href="courses.html">Courses</a></li>
        <li><a href="teachers.html">Teachers</a></li>
        <li><a href="events.html">Events</a></li> -->
        <li class="dropdown active">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Pendidikan</a>
            <ul class="dropdown-menu">
            <li><a href="/tk">TK</a></li>
            <li><a href="/sd">SD</a></li>
            <li><a href="/smp">SMP</a></li>
            <li><a href="/smk">SMK</a></li>
            <!-- <li class="dropdown-submenu dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"><span>Sub Menu</span></a>
                <ul class="dropdown-menu">
                <li><a href="#">Second Level Menu</a></li>
                <li><a href="#">Second Level Menu</a></li>
                <li><a href="#">Second Level Menu</a></li>
                <li><a href="#">Second Level Menu</a></li>
                </ul>
            </li> -->
            </ul>
        </li>
        <li><a href="contact.html">Kontak</a></li>
        <li>
            <button type="button" class="btn btn-primary button-login" data-toggle="modal" data-target="#login" id="openlogin">Login</button>
        </li>
        </ul>
    </div>
    </div>
</nav>
