<script src="js/scripts.min.js"></script>
<script src="js/main.min.js"></script>
<script src="js/custom.js"></script>
<script src="https://use.fontawesome.com/9f73b714c1.js"></script>
<script src="/pathto/js/sweetalert.js"></script>
@include('sweet::alert')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
	jQuery(document).ready(function(){
		jQuery('#ajaxLogin').click(function(e){
			e.preventDefault();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				}
			});
			jQuery.ajax({
				type: "POST",
				url: "/login",
				data: {
					email: jQuery('#emailLogin').val(),
					password: jQuery('#passwordLogin').val(),
				},
				success: function(data){
					if(data){
						if (data.status==false) {
							jQuery('.alert-danger').html('');
							jQuery('.alert-danger').show();
							jQuery('.alert-danger').append('<span>Wrong Email or Password</span>');
						}else if(data.status==true){
						window.location.href = "/user";
						console.log('Login request sent !');
						}else{
							jQuery('.alert-danger').html('');

							jQuery.each(data.errors, function(key, value){
								jQuery('.alert-danger').show();
								jQuery('.alert-danger').append('<li>'+value+'</li>');
							});
						}
					}
				},
			});
		});
	});
	jQuery(document).ready(function(){
		jQuery('#ajaxSubmit').click(function(e){
			e.preventDefault();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				}
			});
			jQuery.ajax({
				url: "{{ url('/regis') }}",
				method: 'post',
				data: {
					nama: jQuery('#nama').val(),
					email: jQuery('#email').val(),
					password: jQuery('#password').val(),
					password_confirmation: jQuery('#password_confirmation').val(),
				},
				success: function(result){
					if(result.errors)
					{
						jQuery('.alert-danger').html('');

						jQuery.each(result.errors, function(key, value){
							jQuery('.alert-danger').show();
							jQuery('.alert-danger').append('<li>'+value+'</li>');
						});
					}
					else
					{
						jQuery('.alert-success').html('');
						jQuery('.alert-success').append('Selamat! Anda sudah berhasil membuat Akun, Silahkan login.');
						jQuery('.alert-danger').hide();
						$('#myModal').modal('hide');
						jQuery('.alert-success').show();
						$('#formRegis').reset();
					}

				}});
		});
	});
</script>
