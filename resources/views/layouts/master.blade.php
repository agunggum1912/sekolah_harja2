<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.css')
    @yield('css_style')
</head>
<body>
    @include('layouts.header')
    @yield('content')
    @include('layouts.footer')
    @include('layouts.js')
    @yield('script')
</body>
</html>
