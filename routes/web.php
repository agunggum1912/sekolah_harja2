<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'FrontendController@index')->name('index');
Route::get('/tk', 'FrontendController@tk')->name('tk');
Route::get('/sd', 'FrontendController@sd')->name('sd');
Route::get('/smp', 'FrontendController@smp')->name('smp');
Route::get('/smk', 'FrontendController@smk')->name('smk');
Route::get('/news', 'FrontendController@news')->name('news');

// Route::post('/register','RegisterController@proses');

// Route::get('/login/user', 'LoginController@getLogin');
Route::get('/login', 'LoginController@getLogin')->middleware('guest');
Route::post('/login','LoginController@postLogin');

Route::get('/logout', 'LoginController@logout');

Route::get('/user', function(){
	return view('user');
})->middleware('auth:user');

Route::resource('regis','RegisterController');

// Route::resource('login','LoginController');